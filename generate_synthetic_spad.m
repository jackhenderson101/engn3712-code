function [syn_data, syn_ground_truth] = generate_synthetic_spad(sequence_dimensions, name, syn_ground_truth)

% assumptions
% Peaks are not spatially correlated in terms of height or timing
% samples are IID
% Distribution of the target doesn't change as the target moves in the
% frame
% background noise is not correlated with target location

sequence_length = sequence_dimensions(3);


pk_thresh = 950;

% read in the reference dataset
load('datasets/movingAwayDark/movingAwayDark1.mat');
data = data(:,:,1:1000);
% Assume the target is stationary in the video.
target = Target(24,14,14,10);

% imagesc(data(:,:,1));
% hold on
% plot(syn_ground_truth.target(1));
% plot(syn_ground_truth.target(end));

% The synthetic dataset
syn_data = 1024 * ones(sequence_dimensions);

%rotate 90 degrees. not that it really matters.
% %%%%%Confirm which orientation is correct;%%%%%
% dataset = dataset'; 



[ylim, xlim] = target.limits();
yrange = ylim(1):ylim(2);
xrange = xlim(1):xlim(2);



% background noise distribution;
% for pixels not in the target
    
bin_edges_vals = 0:1025;
bin_edges_deltas = 0:1000;


% Collect all of the pixels in the background
bg_pixels = [];
for i = 1:size(data,1)
    for j = 1:size(data,2)
        % skip the target region
        if ismember(i, yrange) && ismember(j, xrange)
            continue;
        end
        bg_pixels = [bg_pixels; squeeze(data(i,j,:))];
    end
end

% Generate the histograms for the background pixels
peaks = bg_pixels < pk_thresh;
peak_vals = bg_pixels(peaks);
peak_times = find(peaks);
peak_delta_times = diff(peak_times);

% calculate histograms of the peak heights and the time between
% peaks
peak_hist = histcounts(peak_vals, bin_edges_vals);
delta_hist = histcounts(peak_delta_times, bin_edges_deltas);

% create the sequence with just background noise
for i = 1:size(syn_data,1)
    for j = 1:size(syn_data,2)
        syn_px = inverse_transform_sample(sequence_length, peak_hist, delta_hist, bin_edges_vals, bin_edges_deltas);
        syn_data(i,j,:) = syn_px;
    end
end     

syn_target = 1024 * ones(length(yrange), length(xrange), sequence_length);

for i = 1:length(yrange)
    for j = 1:length(xrange)
        px = squeeze(data(yrange(i),xrange(j),:));
        
        peaks = px < pk_thresh;
        peak_vals = px(peaks);
        peak_times = find(peaks);
        peak_delta_times = diff(peak_times);
       
        
        % calculate histograms of the peak heights and the time between
        % peaks
        peak_hist = histcounts(peak_vals, bin_edges_vals);
        delta_hist = histcounts(peak_delta_times, bin_edges_deltas);
        
        if sum(peak_hist) == 0 || sum(delta_hist) == 0
            continue
        end
        
        syn_px = inverse_transform_sample(sequence_length, peak_hist, delta_hist, bin_edges_vals, bin_edges_deltas);
        
        syn_target(i,j,:) = syn_px;
    end
end

% insert the target onto the background according to the ground truth
for i = 1:sequence_length
   t = syn_ground_truth.target(i);
   [ylim, xlim] = t.limits;
   
   syn_data(ylim(1):ylim(2), xlim(1):xlim(2), i) = syn_target(:,:,i);
end
       
syn_data = uint16(syn_data); 

data = syn_data;
ground_truth = syn_ground_truth;
save(['datasets/synthetic_spad/', name, '.mat'], 'data', 'ground_truth');

end

    
function syn_px = inverse_transform_sample(sequence_length, peak_hist, delta_hist, bin_edges_vals, bin_edges_deltas)
    % Calculate the cumulative distribution, sample uniformly from the y-axis
    % and use the CDF to get an x-value. 
    [~, syn_val_bins] =   histc(rand(1,sequence_length), [0; cumsum(peak_hist') /sum(peak_hist)]);
    [~, syn_delta_bins] = histc(rand(1,sequence_length), [0; cumsum(delta_hist')/sum(delta_hist)]);
    syn_vals = bin_edges_vals(syn_val_bins);
    syn_deltas = bin_edges_deltas(syn_delta_bins);
    syn_times = cumsum(syn_deltas);

    % Delete values beyond the required time length
    syn_vals(syn_times>sequence_length) = [];
    syn_times(syn_times>sequence_length) = [];

    syn_px = 1024 * ones(1, sequence_length);
    syn_px(syn_times) = syn_vals;
end




    