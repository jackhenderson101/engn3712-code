function metrics = evaluate_metrics_on_spad_set( test_file)
    %load the specs from the file
    test_specs = jsondecode(fileread(['tests/' test_file '.json']));
    total_resets = zeros(1, length(test_specs.datasets));
    
    spad_names = fieldnames(test_specs.spad_datasets);
    spad_properties = test_specs.spad_properties;
    for i = 1:length(spad_names)
       data_type = spad_names{i};
       for j = test_specs.spad_datasets.(data_type)'
            metrics = evaluate_metrics(test_file, data_type, true, false, j);
            
%             for j = 1:length(track_results.trackers)
%                 t = track_results.trackers(j);
%                 total_resets(j) = total_resets(j) + length(resets);
%             end
            
            
            if i == 1 && j == 1
                combined_metrics = metrics;
                load(['results/' test_file '/' data_type num2str(j) '.mat'], 'trackers')
                trackers([1,2]) = trackers([2,1]);
            else
                for k = 1:numel(metrics)
                    combined_metrics{k}.vals = vertcat(combined_metrics{k}.vals, metrics{k}.vals);
                end
            end
       end
        
        for m = 1:size(combined_metrics,2)
            combined_metrics{1,m}.plot(combined_metrics(:,m), trackers);
        end
    
end

