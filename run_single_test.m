function run_single_test(dataset, trackers, test_name, show_live_video)
    initial_frame = dataset.next_frame();
    target = dataset.initial_target();
    
    if show_live_video
        colours = {'r', 'b', 'g'};
        if isa(dataset, 'SPADDataSet')
            colormap gray
        end
    end
    
    
    for i = 1:length(trackers)
        trackers(i).tracker.init(initial_frame, target, trackers(i).props);
        trackers(i).history = TargetHistory(dataset.length, target);
    end
    
    while dataset.has_next_frame()
        if mod(dataset.current_frame, 100) == 0
            fprintf('Frame %d/%d\n', dataset.current_frame, dataset.length);
        end
        
        frame = dataset.next_frame();

        for t = trackers
            t.tracker.update(frame);
            t.history.update(dataset.current_frame, t.tracker.target);
            if t.props.target_resetting
               gt_target = dataset.ground_truth.target(dataset.current_frame);
               distance = Metrics.CorrectedLocationError.evaluate(t.tracker.target, gt_target);
               if distance > t.props.reset_threshold
                   fprintf('%s Tracker was reset at frame %d\n', t.name, dataset.current_frame);
                   t.tracker.init(frame, gt_target, t.props);
                   t.history.resets(end+1) = dataset.current_frame;
               end
            end
                
        end
        
        if show_live_video
            clf
            imagesc(frame);
            if isfield(dataset, 'ground_truth')    
                gt = dataset.ground_truth.target(dataset.current_frame);
                plot(gt, 'k');
            end
            for i = 1:length(trackers)
               target = trackers(i).history.target(dataset.current_frame);
               plot(target, colours{1+mod(i, length(colours))});
            end

           drawnow();
       
        end
        
    end
    
    for i = 1:length(trackers)
        trackers(i).tracker.finish();
    end
    
    save(['results/' test_name '/' dataset.name], 'trackers');
end

%            trackers(i).metrics = get_metrics(metric_specs, dataset);
% 
%           for metric = t.metrics
%                metric.evaluate(dataset.current_frame, t.tracker.target, gt_target);
%             end
% function metrics = get_metrics(specs, dataset)
%     num_metrics = length(specs);    
%     for i = 1:num_metrics
%         metrics(i) = eval(['Metrics.' specs{i} '(' num2str(dataset.length) ')']); %#ok<AGROW>
%     end
% end