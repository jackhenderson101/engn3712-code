classdef LocationError < Metrics.Metric
    % Measures Euclidean distance between centre of target and centre of
    % ground truth.
    
    properties
        
    end
    
    methods(Static)
        function score = evaluate(tracker_target, ground_truth)
            [t_y, t_x] = tracker_target.global_position();
            [gt_y, gt_x] = ground_truth.global_position();
            
            score = sqrt( (t_y - gt_y)^2 + (t_x - gt_x)^2);
        end
    end
    
    methods
        function metric = LocationError(total_frames)
            metric = metric@Metrics.Metric(total_frames);
            
        end
        
        
        function plot(metric, all_metrics, all_trackers, subplots)
            if nargin < 4
                subplots = false;
            end
            figure()
            hold on
            for i = 1:length(all_metrics)
                vals = all_metrics{i}.vals;

                if subplots
                    subplot(length(all_trackers), 1, i);
                    hold on
                    if i == length(all_metrics)
                        xlabel("Frame Number");
                    end
                    ylabel("Location Error (px)");
                    title(all_trackers(i).name);
                    xlim([1, length(vals)]);
                end
                plot(vals);
            end
            if ~subplots
                legend(all_trackers.name);
                xlabel("Frame Number");
                ylabel("Location Error (px)");
                title("Location Error");
            end
        end
    end
    
end

