classdef CorrectedLocationError < Metrics.LocationError
    % Measures Euclidean distance between centre of target and centre of
    % ground truth, normalised by the width and height of the ground truth
    % target

    properties
        
    end
    
    methods(Static)
        function score = evaluate(tracker_target, ground_truth)
            [t_y, t_x] = tracker_target.global_position();
            [gt_y, gt_x] = ground_truth.global_position();
            
            delta_y = (t_y - gt_y) / ground_truth.height;
            delta_x = (t_x - gt_x) / ground_truth.width;
            
            score = sqrt( delta_y^2 + delta_x^2);
        end
    end
    
    methods
        function metric = CorrectedLocationError(total_frames)
           metric = metric@Metrics.LocationError(total_frames);
           
        end
        
        function plot(metric, all_metrics, all_trackers, subplots)
            plot@Metrics.LocationError(metric, all_metrics, all_trackers, subplots);
%             ylabel("Location Error (normalised)");
%             title("Corrected Location Error");
%             ylim([0 1.5]);
%             title('')
            figure();
            hold on
           for i = 1:length(all_metrics)
               vals = all_metrics{i}.vals;
               success = precision_curve(vals, metric.line_specs{i});
               fprintf('%s: %.2f\n', all_trackers(i).name, success*100);
           end
            legend(all_trackers.name, 'Location', 'southeast');
        end
    end
    
end

