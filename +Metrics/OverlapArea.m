classdef OverlapArea < Metrics.Metric
    % Measures the area of the overlap between the target rectangle and the
    % ground truth rectangle.
    
    properties
        
    end
    
    methods(Static)
        function score = evaluate(tracker_target, ground_truth)
            t_rect = tracker_target.bounding_box();
            gt_rect = ground_truth.bounding_box();
            
            gt_area = gt_rect(3) * gt_rect(4);
            t_area = t_rect(3) * t_rect(4);
            overlap = rectint(t_rect, gt_rect);
            score = overlap / (gt_area + t_area - overlap);
        end
    end
    
    methods
        function metric = OverlapArea(total_frames)
           metric = metric@Metrics.Metric(total_frames);
           
        end
        

        
        function plot(metric, all_metrics, all_trackers, ~)
%            figure()
%            hold on
%            for i = 1:length(all_metrics)
%                vals = all_metrics{i}.vals;
%                plot(vals);
%            end
%            legend(all_trackers.name);
%            xlabel("Frame Number");
%            ylabel("Fraction Overlap with Ground Truth");
%            title("Target Overlap");
           
           figure();
           hold on
           fprintf('Success Curves');
           for i = 1:length(all_metrics)
               vals = all_metrics{i}.vals;
               success = success_curve(vals, metric.line_specs{i});
              fprintf('%s: %.2f\n', all_trackers(i).name, success*100);

           end
           legend(all_trackers.name);
        end
    end
    
end

