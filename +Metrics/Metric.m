classdef Metric < handle & matlab.mixin.Heterogeneous
    %METRIC Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        total_frames
        vals
        line_specs
    end
    
    methods
        function metric = Metric(total_frames)
            if nargin == 0 
                return
            end
            metric.total_frames = total_frames;
            metric.vals = nan(total_frames, 1);
            metric.line_specs = {'r-', 'g-.', 'b--', 'm:'};
        end
      
        function finalise(metric, resets)
            for r = resets
                % Discard the value for 10 frames after a reset.
                metric.vals(r:r+10) = NaN; 
            end
        end
        
        function update(metric, frame_number, tracker_target, ground_truth)
            metric.vals(frame_number) = metric.evaluate(tracker_target, ground_truth);
        end
        
    end
    
    methods(Abstract, Static)
        evaluate(tracker_target, ground_truth_target); % Evaluate the metric on a single frame 
    end

    
end

