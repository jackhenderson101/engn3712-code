classdef TargetHistory < handle
    properties
        target
        resets
    end
    
    methods
        function history = TargetHistory(total_frames, initial_target)
           history.target = initial_target;
           history.target(total_frames) = Target;
           history.resets = [];
        end
        
        function update(history, frame_number, tracker_target)
            history.target(frame_number) = tracker_target;
        end
    end
    
end

