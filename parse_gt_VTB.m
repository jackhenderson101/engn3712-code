function parse_gt_VTB( name )
    path = ['datasets/' name];
    load([path '/cfg.mat']);
    length = size(seq.gt_rect,1);
    d = DataSet();
    d.name = name;
    d.length = length;
    
    file_names = cell(1,length);
    ground_truth(length) = Target;
    
    for i = 1:length
        file_idx = i;
        file_names{i} = sprintf(seq.img_filename_fmt, file_idx);
        
        gt = seq.gt_rect(i,:);
        gt = max(gt,0);
        
        ground_truth(i) = Target(gt(2)+gt(4)/2, gt(1)+gt(3)/2, gt(4), gt(3));
    end
    
    history = TargetHistory(length, Target);
    history.target = ground_truth;
    d.ground_truth = history;
    d.file_names = file_names;
    dataset = d;
    save([path '/dataset'], 'dataset');
end