classdef Target
    
    properties
        height % Dimensions of the target
        width
        origin_y % The pixel offset of the origin reference point
        origin_x
        y
        x
        h
    end
    
    methods
        function t = Target(origin_y, origin_x, height, width)
            if nargin == 0
                return
            end
           t.origin_y = double(origin_y);
           t.origin_x = double(origin_x);
           t.height = max(double(height),1); % ensure the width and height don't go below 1px
           t.width = max(double(width),1);
           t.x = 0;
           t.y = 0;
           t.h = 1;
        end
        
        function t = candidate_target(target, y, x, h)
           t = Target(target.origin_y, target.origin_x, target.height * h, target.width * h);
           t.y = double(y);
           t.x = double(x);
           t.h = h;
        end
        
        function size = size(t)
            size = [t.height, t.width];
        end
        
        function [y_global, x_global] = global_position(t)
            y_global = t.origin_y + t.y * t.height/2;
            x_global = t.origin_x + t.x * t.width/2;
        end
        
        function [ylim, xlim] = limits(t)
            [y_g, x_g] = global_position(t);
            
            ylim = [y_g - t.height /2, y_g + t.height /2];
            xlim = [x_g - t.width /2 , x_g + t.width /2 ];
        end
        
        function bbox = bounding_box(t)
            [ylim, xlim] = limits(t);
            bbox = [xlim(1), ylim(1), t.width, t.height];
        end
        
        function bbox = bounding_box_reverse(t)
            [ylim, xlim] = limits(t);
            bbox = [ylim(1), xlim(1), t.height, t.width];
        end
        
        function subimage = crop(t, image)
            ymax = size(image,1);
            xmax = size(image,2);
            [ylim, xlim] = limits(t);
            yrange = ceil(ylim(1)+0.0001):floor(ylim(2));
            xrange = ceil(xlim(1)+0.0001):floor(xlim(2));
            
            %If the target extends outside the image, pad it with the image
            %border.
            yrange(yrange < 1) = 1;
            xrange(xrange < 1) = 1;
            yrange(yrange > ymax) = ymax;
            xrange(xrange > xmax) = xmax;
            
            
            
            subimage = image(yrange, xrange, :);
        end
        
        function features = extract_features(t, image, num_bins)
            features = Image_Quantisation.extract_features(image);
        end
        
        
        function [norm_y, norm_x] = normalised_coords(t)
            [ylim, xlim] = limits(t);
            
            y_vals = (ceil(ylim(1)+0.0001):floor(ylim(2))) - t.origin_y; % the pixel coordinates of each pixel in the target
            norm_y = 2 * y_vals ./ t.height; % convert to normalised coordinates
            
            x_vals = (ceil(xlim(1)+0.0001):floor(xlim(2))) - t.origin_x;
            norm_x = 2 * x_vals ./ t.width;
        end
        
        function plot(t, colour)
            if nargin < 2
                colour = 'k';
            end
            rectangle('Position', bounding_box(t), 'EdgeColor', colour);

%             [y_global, x_global] = global_position(t);
%             plot(x_global, y_global, 'k+')
        end
        
        function p_hat = calc_p_hat(t, num_features, features)
            p_hat = zeros(1, num_features);

            [norm_y, norm_x] = normalised_coords(t);
            [x_y, x_x] = meshgrid(norm_y, norm_x);

            k = MeanShift.kernel((t.y - x_y).^2 + (t.x - x_x).^2);
            k = k';

            for i = 1:length(norm_y)
                for j = 1:length(norm_x)
                    % Find the feature, b, of this pixel
                    b = features(i,j);
                    % Add the weighting of this pixel to the probability of the
                    % feature
                    p_hat(b) = p_hat(b) + k(i,j);
                end
            end

            % Normalise the probabilities
            p_hat = p_hat ./ sum(k(:)); 
        end
        
        function y_1 = calc_y_1(t, w)
            [norm_y, norm_x] = normalised_coords(t);
            
            y_1 = [0, 0];
            for i = 1:length(norm_y)
                for j = 1:length(norm_x)
                    x_i = [norm_y(i), norm_x(j)];
                    y_1 = y_1 + x_i .* w(i,j);
                end
            end

            if sum(w(:)) ~= 0
                y_1 = y_1 /sum(w(:));
            end
        end

    end
    
end

