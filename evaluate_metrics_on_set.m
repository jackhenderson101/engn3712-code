function metrics = evaluate_metrics_on_set( test_file)
    %load the specs from the file
    test_specs = jsondecode(fileread(['tests/' test_file '.json']));
    total_resets = zeros(1, length(test_specs.datasets));
    
        for i = 1:length(test_specs.datasets)
            metrics = evaluate_metrics(test_file, test_specs.datasets{i}, false, false);
            track_results = load(['results/' test_file '/' test_specs.datasets{i} '.mat'], 'trackers');
            for j = 1:length(track_results.trackers)
                t = track_results.trackers(j);
                total_resets(j) = total_resets(j) + length(t.history.resets);
            end
            
            
            if i == 1
                combined_metrics = metrics;
                load(['results/' test_file '/' test_specs.datasets{i} '.mat'], 'trackers')
            else
                for j = 1:numel(metrics)
                    combined_metrics{j}.vals = vertcat(combined_metrics{j}.vals, metrics{j}.vals);
                end
            end
        end
        
        
        for m = 1:size(combined_metrics,2)
            combined_metrics{1,m}.plot(combined_metrics(:,m), trackers);
        end

        total_resets
    
end

