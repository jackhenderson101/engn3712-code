sequence_dimensions = [32,32,1000];
height = 14;
width = 10;

% Top left to bottom right
gt_start = [15,15];
gt_finish = [15,15];
syn_ground_truth = linear_trajectory(sequence_dimensions(3), gt_start, gt_finish, height, width);
generate_synthetic_spad(sequence_dimensions, 'synthetic_spad_demo', syn_ground_truth);

p = struct;
p.scale = 1;


p.stride = 1;
p.feature = 'raw';
ds = SPADDataSet('./datasets/synthetic_spad/synthetic_spad_demo.mat', p);
figure
imagesc(ds.next_frame());
colormap gray
axis square
    f = gcf;
    f.Position = [100,100,475,450];
print('-deps2', '-loose', '../engn3712-report/images/spad_feature_raw.eps'); 

for i = [5, 10, 30]
    p.stride = i;
    p.feature = 'peak_count';
    ds = SPADDataSet('./datasets/synthetic_spad/synthetic_spad_demo.mat', p);
    figure
    imagesc(ds.next_frame());
    
    axis square
    colormap gray
    f = gcf;
    f.Position = [100,100,475,450];
    print('-deps2', '-loose', ['../engn3712-report/images/spad_feature_', num2str(i), '.eps']); 
end


function gt = linear_trajectory(sequence_length, start, finish, height, width)
    len = sequence_length;
    gt = TargetHistory(len, Target);
    for i = 1:sequence_length
        % lineraly interpolate between the start and the finish
        pos  = (len - i - 1)/(len -1) * start + (i-1)/(len-1) * finish;
        pos = round(pos);
        t = Target(pos(1), pos(2), height, width);
        gt.update(i, t);
    end
end
