function run_test(test_file, force_rerun, show_live_video)
    if nargin == 0
        test_file = 'test_default';
        fprintf("No test file specified. Using default file: %s\n", test_file);
    end
    if nargin < 2
        force_rerun = false;
    end
    if nargin < 3
        show_live_video = false;
    end
    
    %load the specs from the file
    test_specs = jsondecode(fileread(['tests/' test_file '.json']));
    
    %ensure that all the required fields have been set
    assert(all(isfield(test_specs, {'datasets', 'spad_datasets', 'trackers'})));
    
    num_trackers = length(test_specs.trackers);
    
    % Initialise the datasets
    datasets(1) = DataSet;
    for i = 1:length(test_specs.datasets)
        load(['datasets/' test_specs.datasets{i} '/dataset.mat'], 'dataset');
        datasets(end+1) = dataset;
        fprintf('Loaded Dataset: %s\n', dataset.name);
        im_dim = dataset.image_dimensions();
        target_dim = dataset.average_target_size;
        fprintf('Dataset Properties:\nLength: %d\nDimensions: %d x %d\nAvg Target Size: %d x %d\n',...
            dataset.length,...
            im_dim(1), im_dim(2), ...
            target_dim(1), target_dim(2) ...
            );
    end
    
    spad_names = fieldnames(test_specs.spad_datasets);
    spad_properties = test_specs.spad_properties;
    for i = 1:length(spad_names)
       data_type = spad_names{i};
       for j = test_specs.spad_datasets.(data_type)'
           dataset = SPADDataSet(['./datasets/' data_type '/' data_type num2str(j) '.mat'], spad_properties);
           datasets(end+1) = dataset;
       end
    end
    datasets(1) = [];
    num_datasets = length(datasets);
    fprintf('Loaded %d datasets\n\n', num_datasets);
    
    % Initialise the trackers
    trackers = struct('name', {}, 'tracker', {}, 'props', {});
    for i = 1:num_trackers
        if num_trackers == 1
            t_specs = test_specs.trackers;
        else
            t_specs = test_specs.trackers{i};
        end
        
        
        t = struct;
        t.name = t_specs.name;
        t.tracker = eval(t_specs.tracker); % Must specifiy the fully-qualified class name;
        t.props = get_tracker_properties(t.tracker, t_specs);
        trackers(i) = t; 
        fprintf('Loaded Tracker: %s\n', t.name)
    end
    
    fprintf('Loaded %d trackers\n\n', num_trackers);
    
    % create a directory for the results
    [~,~,~] = mkdir('results', test_file); % Suppress the error if the directory already exists.
    disp('Running Tests...');
    for i = 1:num_datasets
        d = datasets(i);
        fprintf('Starting dataset: %s\n', d.name);

        file = ['results/' test_file '/' d.name '.mat'];
        if exist(file, 'file') == 2 && ~force_rerun
            disp('Skipping dataset as it already has a result file. Call function with "force_rerun" to override');
            continue
        end
        try
            run_single_test(d, trackers, test_file, show_live_video);
            fprintf('Finished dataset: %s\n', d.name);
        catch EX
            fprintf('Dataset Failed: %s\n', d.name);
            disp(getReport(EX, 'extended'));
        end

    end
    

end

% Start with the default properties of the tracker and  make any changes
% specified in the spec file.
function props = get_tracker_properties(tracker, specs)
    props = tracker.default_properties();
    names = fieldnames(specs);
    for i = 1:length(names)
        key = names{i};
        if strcmp(key, 'name') || strcmp(key, 'tracker')
            continue
        end
        val = specs.(key);
%         assert(isfield(props, key)); 
        props.(key) = val;
    end
end

