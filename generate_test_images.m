clear variables;
target = ones(11,11, 3);
background_image = zeros(200,200, 3);
dataset_name = 'test02';

% Coordinates of the top-left corner of the target
xtrack = 1:3:190;
ytrack = 1:3:190;

mkdir('../datasets', dataset_name);

for i = 1:length(xtrack)
   image = background_image;
   xrange = xtrack(i):(xtrack(i) + size(target,1) - 1);
   yrange = ytrack(i):(ytrack(i) + size(target,2) - 1);
   image(yrange, xrange, :) = target;
   imwrite(image, sprintf('../datasets/%s/frame%04d.png', dataset_name, i-1));
    
end