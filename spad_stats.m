close all;
num_seqs = 55;
% all_data = [];
% for i=1:num_seqs
%    filename = ['./datasets/movingAwayDark/movingAwayDark', num2str(i), '.mat'];
%    load(filename);
%    if i == 1
%        single_data = data;
%    end
%    all_data = cat(3, all_data, data);
% end
figure
imagesc(mean(single_data,3));
title('Mean Image')

% Average Peak height
avg_peak = zeros(32,32);
peaks_freq = zeros(32,32);
for i=1:size(all_data,1)
    for j = 1:size(all_data,2)
        px = single_data(i,j,:);
        peaks = px(px<950);
        peaks_freq(i,j) = sum(px<950, 3);
        avg_peak(i,j) = mean(peaks);
    end
end
figure
imagesc(avg_peak)
title('Average Peak Height');

figure
imagesc(peaks_freq)
title(' Peak Frequency');

pixel_x = 15;
pixel_y = 16;

%pixel histogram
figure
px = single_data(pixel_x,pixel_y,:);
peaks = px(px<950);
histogram(peaks);
title(sprintf('Histogram of peaks px(%d, %d)', pixel_x, pixel_y));

% figure
% px = single_data(pixel_x,pixel_y,:);
% peaks = px(px<950);
% histogram(peaks);
% title(sprintf('Histogram of peaks px(%d, %d)', pixel_x, pixel_y));

% Frequency over time

px = squeeze(single_data(pixel_x,pixel_y,:));
thresh = px<950;
peaks = [];
peaks_time = [];
bin_size = 20;
freq = [];
avg_height = [];
avg_peak_height = [];
binned_mean = [];
cog_x = [];
cog_y = [];
%%
figure
for i=1:size(single_data, 3)/bin_size
    t_range = (i-1)*bin_size+1:i*bin_size;
    
    binned_mean_i = mean(single_data(:,:,t_range)< 950, 3);
    binned_mean(:,:,i) = binned_mean_i;
    [max_val, max_idx] = max(binned_mean_i(:));
    
    [cog_x(i), cog_y(i)] = ind2sub(size(binned_mean_i), max_idx);
    clf
    imagesc(binned_mean(:,:,i));
    hold on
    plot(cog_y(i), cog_x(i), 'r*');
    drawnow
    centre_px = squeeze(single_data(round(cog_x(i)), round(cog_y(i)),t_range));
    px_range = centre_px;
    peaks_range = px_range(px_range < 950);
    peaks = vertcat(peaks, peaks_range);
    peaks_time = vertcat(peaks_time, t_range(px_range < 950)');
    freq(i) = sum(px_range < 950);
    avg_peak_height(i) = mean(peaks_range);
end

%%
figure
subplot(2,1,1);
plot((1:length(freq))*bin_size, freq);
title('Peak frequency over time');
xlabel('Frame')
ylabel(sprintf('Count of peaks per %d frames', bin_size));
subplot(2,1,2)

hold on
peaks = double(peaks);
plot(peaks_time, peaks, '.');
X = [peaks_time ones(size(peaks_time,1),1)];
m = X\peaks; % linear regression over peak heights
vals = [0, length(px)];
plot(vals, polyval(m, vals));
title(sprintf('Peak Height over time: %.3e x + %.4g', m(1), m(2)));
mean_vals = polyval(m, peaks_time);
normalised_vals = peaks - mean_vals;
figure
subplot(1,2,1)
histogram(normalised_vals, min(normalised_vals):max(normalised_vals));
title('Histogram of peaks, normalised to moving mean');
subplot(1,2,2)
histogram(peaks, min(peaks):max(peaks));
title('Histogram, non-normalised');
bin_size = 100;
max_i = ceil(size(data,3)/bin_size);


