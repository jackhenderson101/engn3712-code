function [success_threshold] = precision_curve( location_error, line_spec )
    success = 0.25;
    max_error = 1.2;


    location_error(isnan(location_error)) = [];
    threshold = sort(location_error);
    threshold(threshold > max_error) = [];
    threshold(isnan(threshold)) = [];
    threshold(end+1) = max_error;
    precision = linspace(0,length(threshold)/length(location_error),length(threshold));
    precision(end) = precision(end -1);
    plot(threshold, precision, line_spec, 'LineWidth', 2);
    xlabel('Threshold');
    ylabel('Precision');
    ylim([0,1])
    
    success_threshold = precision(find(threshold<success, 1, 'last' ));
    
    
end

