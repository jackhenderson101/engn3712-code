function metrics = evaluate_metrics( test_name, dataset_name, is_spad, show_plot, spad_num)
    if nargin < 4
        show_plot = true;
    end
    test_specs = jsondecode(fileread(['tests/' test_name '.json']));
    
    if is_spad
        load(['results/' test_name '/' dataset_name num2str(spad_num) '.mat'], 'trackers')
        warning('Reordering the trackers');
        trackers([1,2]) = trackers([2,1]); % Reorder the trackers to match the original ordering : Meanshift, KCF, Struck
        dataset = SPADDataSet(['./datasets/' dataset_name '/' dataset_name num2str(spad_num) '.mat'], test_specs.spad_properties);
    else
        load(['results/' test_name '/' dataset_name '.mat'], 'trackers')
        load(['datasets/' dataset_name '/dataset.mat'], 'dataset');
    end
    num_metrics = 3;
    metrics = cell(length(trackers), num_metrics);
    for i = 1:length(trackers)
        metrics{i,1} = Metrics.LocationError(dataset.length);
        metrics{i,2} = Metrics.CorrectedLocationError(dataset.length);
        metrics{i,3} = Metrics.OverlapArea(dataset.length);
    end
        
    for i = 1:length(dataset.ground_truth.target)
        gt = dataset.ground_truth.target(i);
        for t = 1:size(metrics,1)
           target = trackers(t).history.target(i);
           for m = 1:size(metrics,2)
              metrics{t,m}.update(i, target, gt); 
           end
        end
    end
    
    for t = 1:size(metrics,1)   
        resets = trackers(t).history.resets;
        for m = 1:size(metrics,2)
            metrics{t,m}.finalise(resets); % allow the metric to do any post-processing
        end
    end
    
    for m = 1:size(metrics,2)
        if show_plot
            metrics{1,m}.plot(metrics(:,m), trackers, true);
        end   
    end
    
end

