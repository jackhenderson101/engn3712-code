sequence_dimensions = [50,50,1000];
height = 14;
width = 10;

% Top left to bottom right
gt_start = [15,15];
gt_finish = [35,35];
syn_ground_truth = linear_trajectory(sequence_dimensions(3), gt_start, gt_finish, height, width);
generate_synthetic_spad(sequence_dimensions, 'synthetic_spad1', syn_ground_truth);

% vertical down the centre
gt_start = [25,15];
gt_finish = [25,35];
syn_ground_truth = linear_trajectory(sequence_dimensions(3), gt_start, gt_finish, height, width);
generate_synthetic_spad(sequence_dimensions, 'synthetic_spad2', syn_ground_truth);

% horizontal across the centre
gt_start = [15,25];
gt_finish = [35,25];
syn_ground_truth = linear_trajectory(sequence_dimensions(3), gt_start, gt_finish, height, width);
generate_synthetic_spad(sequence_dimensions, 'synthetic_spad3', syn_ground_truth);

% circle anti-clockwise
gt_centre = [25,25];
radius = 15;
speed = 1;
syn_ground_truth = circular_trajectory(sequence_dimensions(3), gt_centre, radius, speed, height, width);
generate_synthetic_spad(sequence_dimensions, 'synthetic_spad4', syn_ground_truth);

%circle clockwise
speed = -1;
syn_ground_truth = circular_trajectory(sequence_dimensions(3), gt_centre, radius, speed, height, width);
generate_synthetic_spad(sequence_dimensions, 'synthetic_spad5', syn_ground_truth);


function gt = circular_trajectory(sequence_length, centre, radius, speed, height, width)
    len = sequence_length;
    gt = TargetHistory(len, Target);
    for i = 1:sequence_length
        % lineraly interpolate between the start and the finish
        pos(1) = centre(1) + radius * sin(speed * i / len * 2 * pi);
        pos(2) = centre(2) + radius * cos(speed * i / len * 2 * pi);
        pos = round(pos);
        t = Target(pos(1), pos(2), height, width);
        gt.update(i, t);
    end
end

function gt = linear_trajectory(sequence_length, start, finish, height, width)
    len = sequence_length;
    gt = TargetHistory(len, Target);
    for i = 1:sequence_length
        % lineraly interpolate between the start and the finish
        pos  = (len - i - 1)/(len -1) * start + (i-1)/(len-1) * finish;
        pos = round(pos);
        t = Target(pos(1), pos(2), height, width);
        gt.update(i, t);
    end
end
        