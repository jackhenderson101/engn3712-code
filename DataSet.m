classdef DataSet < handle & matlab.mixin.Heterogeneous
    %DATASET Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        name
        current_frame
        length
        ground_truth
        file_names
    end
    
    methods
        function d = DataSet()
            d.current_frame = 0;
        end
        
        function b = has_next_frame(d)
            b = d.current_frame < d.length;
        end
        
        function frame = next_frame(d)
           d.current_frame = d.current_frame + 1;
           path = sprintf('datasets/%s/%s', d.name, d.file_names{d.current_frame});
           frame = imread(path);
           if size(frame,3) == 1
               frame = repmat(frame, 1,1,3);
           end
        end
        
        function target = initial_target(d)
            target = d.ground_truth.target(1);
        end
        
        
        
        function dim = image_dimensions(d)
            path = sprintf('datasets/%s/%s', d.name, d.file_names{1});
            frame = imread(path);
            dim(1) = size(frame,1);
            dim(2) = size(frame,2);
        end
        
        function dim = average_target_size(d)
            all_w = zeros(1, d.length);
            all_h = zeros(1, d.length);
            
            for i = 1:d.length
                all_w(i) = d.ground_truth.target(i).width;
                all_h(i) = d.ground_truth.target(i).height;
            end
            dim(1) = round(mean(all_w));
            dim(2) = round(mean(all_h));
        end
        
    end
    
end

