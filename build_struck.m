function build_struck(compile_all)
 if nargin == 0
     compile_all = false;
 end
 try
 StruckOCV('unlock')
 catch
     
 end
 if compile_all
     cd +STRUCK/struck-master/src
     try
        mexOpenCV -g -I../lib opencv_highgui310.lib Config.cpp Tracker.cpp ImageRep.cpp Sampler.cpp HaarFeatures.cpp HaarFeature.cpp RawFeatures.cpp HistogramFeatures.cpp MultiFeatures.cpp LaRank.cpp Features.cpp GraphUtils/GraphUtils.cpp -c
     catch
         fprintf('Could not compile libs');
     end
     cd ../../../
 end
 
 mexOpenCV -g -I+STRUCK/struck-master/lib opencv_highgui310.lib  +STRUCK/struck-master/src/*.obj   +STRUCK/struck-master/src/StruckOCV.cpp
end