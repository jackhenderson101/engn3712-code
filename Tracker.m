classdef (Abstract) Tracker < handle & matlab.mixin.Heterogeneous
    %TRACKER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods(Abstract, Static)
        default_properties()
    end
    
    methods(Static)
        function props = base_properties()
           props = struct;
           props.target_resetting = true;
           props.reset_threshold = 1;
        end
    end
    
    methods(Abstract)
        init(tracker, frame, target, props)
        
        update(tracker, frame)
        
    end
    
    methods
        function finish(tracker)
            
        end
    end
    
end

