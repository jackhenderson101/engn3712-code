classdef Properties
    %PROPERTIES Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        quantisation_bins   % Number of bins to quantise each colour channel into
        max_iterations      % Maximum number of mean-shift iterations
        gamma               % Controls how quickly the scale can change
        colour_space
    end
    
    methods
        function p = Properties()
           p.quantisation_bins = [16, 16, 8];
           p.max_iterations = 20;
           p.gamma = 0.05;
           p.colour_space = 'bg,gr,rgb';
        end
    end
    
end

