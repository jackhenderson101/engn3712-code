classdef Image_Quantisation
    %IMAGE_QUANTISATION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        m1
        m2
        m3
        num_features
        bins
        colour_space
    end
    
    methods
        function iq = Image_Quantisation(image, bins, colour_space)
            import MeanShift.Image_Quantisation.*;

            if nargin <= 1
                iq.bins = [8 8 4];
            else
                iq.bins = bins;
            end
            
            if nargin <= 2
                iq.colour_space = 'gb,gr,rgb';
            else
                iq.colour_space = colour_space;
            end
            
            switch iq.colour_space
                case 'bg,gr,rgb'
                    [c1, c2, c3] = convert_colour_space(image);
                    iq.m1 = quantisation_map(c1, iq.bins(1));
                    iq.m2 = quantisation_map(c2, iq.bins(2));
                    iq.m3 = quantisation_map(c3, iq.bins(3));
                case 'r,g,b'
                    c1 = image(:,:,1);
                    c2 = image(:,:,2);
                    c3 = image(:,:,3);
                    iq.m1 = quantisation_map(c1, iq.bins(1));
                    iq.m2 = quantisation_map(c2, iq.bins(2));
                    iq.m3 = quantisation_map(c3, iq.bins(3));
                case 'gray'
                    c1 = image(:,:,1);
                    iq.m1 = quantisation_map(c1, iq.bins(1));
                otherwise
                    error('invalid colour space');
            end
            
            iq.num_features = prod(iq.bins);
                        
        end
        
        function q = extract_features(iq, target, frame)
            target_image = crop(target, frame);

            switch iq.colour_space
                case 'bg,gr,rgb'
                    [c1, c2, c3] = MeanShift.Image_Quantisation.convert_colour_space(target_image);
                    
                    q1 = iq.m1(c1);
                    q2 = iq.m2(c2);
                    q3 = iq.m3(c3);

                    q = (((q1 - 1) * iq.bins(2)) + (q2-1)) * iq.bins(3) + q3;
                case 'r,g,b'
                    c1 = target_image(:,:,1)+1;
                    c2 = target_image(:,:,2)+1;
                    c3 = target_image(:,:,3)+1;
                    
                    q1 = iq.m1(c1);
                    q2 = iq.m2(c2);
                    q3 = iq.m3(c3);

                    q = (((q1 - 1) * iq.bins(2)) + (q2-1)) * iq.bins(3) + q3;
                case 'gray'
                    c1 = target_image(:,:,1)+1;
                    q = iq.m1(c1);
            end
        end      
    end
    
    methods(Static)
        
        % Create 3 channels from the original RGB colour space
        function [c1, c2, c3] = convert_colour_space(image)
            image = int16(image);
            c1 = uint8((image(:,:,3) - image(:,:,2) + 255)/2); %B-G
            c2 = uint8((image(:,:,2) - image(:,:,1) + 255)/2); %G-R
            c3 = uint8(sum(image,3)/3); %R+G+B
        end
        
        function map = quantisation_map(channel, bins)
            hist = histcounts(channel(:), 256, 'BinLimits', [0,256]);
            hist(1) = 0;
            hist(end) = 0; % Ignore fully black or white pixels
            cum_hist = cumsum(hist);
            map = floor((bins-1) * cum_hist / max(cum_hist) + 0.5) + 1;
        end
    end
    
end

