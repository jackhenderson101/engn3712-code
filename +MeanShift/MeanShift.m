classdef MeanShift < Tracker
    %MEANSHIFT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        props
        feature_extractor
        h
        target
        current_frame
        current_features
    end
    
    methods(Static)
        function p = default_properties()
            p = Tracker.base_properties();
            p.quantisation_bins = [16, 16, 8];
            p.max_iterations = 20;
            p.gamma = 0.05;
            p.colour_space = 'bg,gr,rgb';
            p.scale_adapt = false;
        end
    end
    
    methods
        function init(t, frame, target, props)
            t.props = props;
            t.feature_extractor = MeanShift.Image_Quantisation(frame, props.quantisation_bins, props.colour_space);
            t.h = 1;
            t.target = target;
            t.current_frame = frame;            
            % Get the feature description of the target in the first frame
            t.current_features = t.feature_extractor.extract_features(target, frame);
        end
        
        function update(t, frame)
            q_hat = t.calc_q_hat(t.current_features, t.feature_extractor.num_features);
            if t.props.scale_adapt
                delta_h = 0.1 * t.h;
            else
                delta_h = 0;
            end
            test_h = [t.h - delta_h, t.h, t.h + delta_h];
            similarity_h = [0,0,0];
            cand_targets_h(3) = Target();
            
            for i = 1:3
                new_h = test_h(i);
                [cand_target, similarity] = t.meanshift(new_h, frame, q_hat);
                similarity_h(i) = similarity;
                cand_targets_h(i) = cand_target;
                
            end
            
            similarity_h(2) = similarity_h(2) * 1.05 + 1e-10; % for tie breaking in artificial video sequences
            [~, best_h] = max(similarity_h);
            gamma = t.props.gamma;
            t.h = gamma*test_h(best_h) + (1-gamma)*t.h;
            cand_target = cand_targets_h(best_h);
            [center_y, center_x] = cand_target.global_position();
            t.target = Target(center_y, center_x, cand_target.height, cand_target.width);
            
        end
        
        function [cand_target, similarity] = meanshift(t, h, frame, q_hat)
            cand_target = candidate_target(t.target, 0, 0, h); % Initialise the candidate target to 0,0 relative to the current target
            epsilon = min(1./size(cand_target));
            
            y_1 = [0 0];
            y_1_dist = nan;
            
            for z = 1:t.props.max_iterations
                cand_features = t.feature_extractor.extract_features(cand_target, frame);
                p_hat = cand_target.calc_p_hat(t.feature_extractor.num_features, cand_features);
                w = t.weight_vector(q_hat, p_hat, cand_features);
                
                y_0 = y_1;
                y_0_dist = y_1_dist;
                y_1 = calc_y_1(cand_target, w);
                y_1_dist = t.bhatta_dist(p_hat, q_hat);
                
                y_1s(z,:) = y_0;
                dists(z) = t.bhatta_similarity(p_hat, q_hat);
                cand_target = t.target.candidate_target(y_1(1), y_1(2), h);

                if norm(y_1 - y_0) < epsilon
                    break
                end
            end
            
            % Get the final similarity measure
            cand_features = t.feature_extractor.extract_features(cand_target, frame);
            p_hat = cand_target.calc_p_hat(t.feature_extractor.num_features, cand_features);
            similarity = t.bhatta_similarity(p_hat, q_hat);


        end
        
        %The probablility of a feature u in the target model
        function q_hat = calc_q_hat(t, target_features, num_features)
            q_hat = zeros(1,num_features);
            % x_star is the normalized pixel locations in the region defined as the
            % target model, centered at zero
            [x_star_y, x_star_x] = meshgrid(-1:2/(size(target_features,1)-1):1, -1:2/(size(target_features,2)-1):1);
            k = MeanShift.kernel(x_star_y.^2 + x_star_x.^2);
            k = k';

            %close all; surf(k)

            for i = 1:size(target_features,1)
                for j = 1:size(target_features,2)
                   b = target_features(i,j); % get the feature index of this pixel
                   q_hat(b) = q_hat(b) + k(i,j);
                end
            end

            % Normalise the sum of q_hat to 1
            if sum(k(:)) ~= 0
                q_hat = q_hat./sum(k(:));
            end
        end



        function d = bhatta_dist(t, p_hat, q_hat)
            d = sqrt(1 - sum(sqrt(p_hat.*q_hat)));
        end

        function w = weight_vector(t, q_hat, p_hat, cand_target_features)
            w = zeros(size(cand_target_features));

            for i = 1:size(w,1)
                for j = 1:size(w,2)
                    feature = cand_target_features(i,j);
                    if p_hat(feature) == 0 || q_hat(feature) == 0
                        %disp(['p/q_hat=0 at ', num2str(i),',', num2str(j)]);
                        % avoid a divide by zero error
                        w(i,j) = 0;
                    else
                        w(i,j) = sqrt(q_hat(feature)/p_hat(feature));
                    end
                end
            end
        end

        function d = bhatta_similarity(t, p_hat, q_hat)
            d = sum(sqrt(p_hat.*q_hat));
        end
    end
    
end

