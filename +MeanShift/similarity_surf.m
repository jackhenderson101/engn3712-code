function similarity_surf(q_hat, frame2, target)
    num_bins=16; % number of bins each colour channel is quantised into
    num_features = num_bins^3; % 3 colour channels
    grid_size = 40;
    y_vals = linspace(-0.5, 0.5, grid_size);
    x_vals = linspace(-0.5, 0.5, grid_size);
    h = 1;
    dists = zeros(grid_size);

    for i = 1:grid_size
        for j = 1:grid_size
            y = y_vals(i);
            x = x_vals(j);
            cand_target = candidate_target(target, y, x, h);
            cand_features = extract_features(cand_target, frame2, num_bins);
            p_hat_f2 = calc_p_hat(cand_target, num_features, cand_features);
            dists(i,j) = bhatta_dist(p_hat_f2, q_hat);
        end
    end

    surf(y_vals, x_vals, dists)

end

function d = bhatta_dist(p_hat, q_hat)
    d = sum(sqrt(p_hat.*q_hat));
end
        