classdef BlobTrack < Tracker
    
    properties
        props
        target
    end
    
    methods(Static)
        function p = default_properties()
            p = Tracker.base_properties();
        end
    end
    
    methods
        function init(t, frame, target, props)
            t.props = props;
            t.target = target;
        end
        
        function update(t, frame)            
            frame(frame < 50) = 0;
            [y, x] = COG(frame);
            t.target = Target(y, x, t.target.height, t.target.width);
            
        end
                    
    end
end

