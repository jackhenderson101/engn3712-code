classdef SPADDataSet < DataSet
    %SPADDATASET Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        props
        data
    end
    
    methods
         function d = SPADDataSet(filename, props)
            vars = load(filename);
            [~, d.name, ~] = fileparts(filename);

            d.data = vars.data;
            if isfield(vars, 'ground_truth')
                d.ground_truth = vars.ground_truth;
            else
               target = Target(24,14,14,10);
               d.ground_truth = TargetHistory(1,target); 
               d.ground_truth.update(1, target);
            end
            if props.scale ~= 1
                s = props.scale;
                for i = 1:length(d.ground_truth.target)
                    gt = d.ground_truth.target(i);
                    d.ground_truth.target(i) = Target(gt.origin_y*s, gt.origin_x*s, gt.height*s, gt.width*s);
                end
            end
            
            d.props = props;
            d.length = size(d.data, 3) - d.props.stride + 1;
         end
         
         function frame = next_frame(d)
           d.current_frame = d.current_frame + 1;
           switch d.props.feature
               case "raw"
                   frame = d.raw_feature;
               case "median"
                   frame = d.median_feature();
               case "peak_count"
                   frame = d.peaks_count_feature;
               case "peak_count_conv"
                   frame = d.peaks_count_conv_feature;
               otherwise
                   error(['Unknown SPAD feature' d.props.feature]);
           end
           
           if d.props.scale ~= 1
              frame = imresize(frame, d.props.scale, 'nearest');
           end
                   
           
         end
         
         function img = raw_feature(d)
             img = d.data(:,:,d.current_frame);
             img = uint8(img/4);
         end
         
         function img = median_feature(d)
            range = d.get_stride_range;
            img = uint8(median(d.data(:,:,range), 3)/4);
         end

         function img = peaks_count_feature(d)
            range = d.get_stride_range;
            frames = d.data(:,:,range);
             
            img = uint8(sum(frames < 950, 3) * 2^8 / d.props.stride);
         end
         
          function img = peaks_count_conv_feature(d)
              img = peaks_count_feature(d);
              k = [0,1,0;1,1,1;0,1,0]/5;
              img = uint8(conv2(img, k, 'same'));
         end
         
         function r = get_stride_range(d)
            r = d.current_frame:(d.current_frame+d.props.stride-1);
         end

    end
    
end

