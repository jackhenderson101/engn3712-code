function parse_gt_vivid( name, length )
    path = ['datasets/' name];
    
    d = DataSet();
    d.name = name;
    d.length = length;
    
    file_names = cell(1,length);
    ground_truth(length) = Target;
    
    for i = 1:length
        file_idx = i-1;
        file_names{i} = sprintf('frame%05d.jpg', file_idx);
        
        frame1 = file_idx - mod(file_idx, 10);
        if mod(file_idx, 10) == 0
            frame2 = frame1;
        else
            frame2 = frame1 + 10;
        end
        delta = (file_idx - frame1)/10;

        gt_file = fopen(sprintf('%s/%s_masks/mask%05d.txt', path, name, frame1));
        assert(gt_file > 0)

        textscan(gt_file, '%u', 1);
        gt_bbox = textscan(gt_file, '%u %u %u %u', 1);
        [f1_y1, f1_y2, f1_x1, f1_x2] = gt_bbox{:};
        fclose(gt_file);

        gt_file = fopen(sprintf('%s/%s_masks/mask%05d.txt', path, name, frame2));
        assert(gt_file > 0)
        
        textscan(gt_file, '%u', 1);
        gt_bbox = textscan(gt_file, '%u %u %u %u', 1);
        [f2_y1, f2_y2, f2_x1, f2_x2] = gt_bbox{:};
        fclose(gt_file);

        y1 = (1-delta) * f1_y1 + delta * f2_y1;
        y2 = (1-delta) * f1_y2 + delta * f2_y2;
        x1 = (1-delta) * f1_x1 + delta * f2_x1;
        x2 = (1-delta) * f1_x2 + delta * f2_x2;

        ground_truth(i) = Target((y1+y2)/2, (x1+x2)/2, y2-y1, x2-x1);
    end
    
    history = TargetHistory(length, Target);
    history.target = ground_truth;
    d.ground_truth = history;
    d.file_names = file_names;
    dataset = d;
    save([path '/dataset'], 'dataset');
end