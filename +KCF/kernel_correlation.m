function [ correlation_f ] = kernel_correlation( x1, x2, sigma )

%     N = numel(x1); % note, unsure why there is normalisation here
%     x1_sqnorm = sum(conj(x1(:)) .* x1(:)) / N;
%     x2_sqnorm = sum(conj(x2(:)) .* x2(:)) / N;
%     
%     x1x2 = sum( conj(x1) .* x2, 3);
%     % Note, reference implementations conjugate the second term... why??? Does it make a difference?
%     % Note, reference implementation performs the sum after the ifft, why?
%     
%     correlation = exp( -1/(sigma^2) * (x1_sqnorm + x2_sqnorm - 2 * ifft2(x1x2)) / N);
% 
%     correlation_f = fft2(correlation);
    xf = x1;
    yf = x2;
    
    %%%%%%%%%%%%

	N = size(xf,1) * size(xf,2);
	xx = xf(:)' * xf(:) / N;  %squared norm of x
	yy = yf(:)' * yf(:) / N;  %squared norm of y
	
	%cross-correlation term in Fourier domain
	xyf = xf .* conj(yf);
	xy = sum(real(ifft2(xyf)), 3);  %to spatial domain
	
	%calculate gaussian response for all positions, then go back to the
	%Fourier domain
	kf = fft2(exp(-1 / sigma^2 * max(0, (xx + yy - 2 * xy) / numel(xf))));

    %%%%%%%%%%%%%%%%
    
    correlation_f = kf;
end

