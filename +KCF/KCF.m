classdef KCF < Tracker
    %KCF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        target
        props
        alpha_f
        target_features
        
        gaussian_pdf
        cosine_window
    end
    
    methods(Static)
        function p = default_properties()
            p = Tracker.base_properties();
            p.feature_bandwidth = 0.2;
            p.training_rate = 0.075;
            p.spatial_bandwidth = nan;
            p.lambda = 10^-4;
            p.padding = 2.5;
            p.scale_step = 1.05;
            p.scale_weight = 0.95; % Weight different scales lower than than constant scale
        end
    end
    
    methods
        function tracker = KCF()
            
        end
        
        function init(tracker, frame, target, props)
            tracker.target = target;
            % Init parameters
            props.spatial_bandwidth = sqrt(target.width * target.height) / 10;
            tracker.props = props;

            
            pt = tracker.padded_target;
            tracker.alpha_f = zeros(pt.height, pt.width);
            tracker.target_features = zeros(pt.height, pt.width);
            
            tracker.cosine_window = hann(pt.height) * hann(pt.width)';
            tracker.gaussian_pdf = KCF.create_target_labels(pt, tracker.props.spatial_bandwidth);
            
            features = tracker.get_features(pt, frame);
            % Train model on initial target location
            tracker.train(features, 1);
            
            
        end
        
        function update(tracker, frame)
            % Perform bounds check on target
            best_response = 0;
            best_scale = 0;
            scale_step = tracker.props.scale_step;
            
            if scale_step == 0
                scale_tests = 1;
            else
                scale_tests = [1/scale_step, 1, scale_step];
            end
            
            for scale = scale_tests
                test_target = tracker.padded_target.candidate_target(0,0,scale);
                
                % Get features of new frame
                features = tracker.get_features(test_target, frame);

                % Detect position of target in new frame
                [new_target, response] = tracker.detect(tracker.target_features, features);
                if scale ~= 1
                    response = tracker.props.scale_weight * response;
                end
                if response > best_response
                    best_response = response;
                    best_scale = scale;
                    best_target = new_target.candidate_target(0,0,scale);
                end
            end
            s = best_target.size;
%             fprintf("Target: %d x %d\n", s(1), s(2));
            
            
            % Update position of target
            tracker.target = best_target;
            % Get the features at the target's new location
            new_features = tracker.get_features(tracker.padded_target, frame);
                        
            % Train model based on new target location
            tracker.train(new_features, tracker.props.training_rate);

        end
        
        function [new_target max_response] = detect(tracker, target_features, image_features)
            % detect the target in the next frame
            k_f = KCF.kernel_correlation(image_features, target_features, tracker.props.feature_bandwidth);
            responses = real(ifft2(tracker.alpha_f .* k_f));
            
            %target location is at the maximum response. we must take into
			%account the fact that, if the target doesn't move, the peak
			%will appear at the top-left corner, not at the center (this is
			%discussed in the paper). the responses wrap around cyclically.
            max_response = max(responses(:));
			[vert_delta, horz_delta] = find(responses == max_response, 1);
            if vert_delta > size(target_features,1) / 2  %wrap around to negative half-space of vertical axis
				vert_delta = vert_delta - size(target_features,1);
            end
            
            if horz_delta > size(target_features,2) / 2  %same for horizontal axis
				horz_delta = horz_delta - size(target_features,2);
            end
            
            new_target = tracker.target;
            new_target.origin_y = new_target.origin_y + vert_delta - 1;
            new_target.origin_x = new_target.origin_x + horz_delta - 1;
                        
        end
        
        function train(tracker, new_target_features, training_rate)
            k_hat_xx = KCF.kernel_correlation(new_target_features, new_target_features, tracker.props.feature_bandwidth);
            % Based on Eq 17:
            new_alpha_f = tracker.gaussian_pdf ./ (k_hat_xx + tracker.props.lambda);
            
            % Update alpha_f based on the training rate
            tracker.alpha_f = (1 - training_rate) * tracker.alpha_f + ...
                              training_rate * new_alpha_f;
            tracker.target_features = (1 - training_rate) * tracker.target_features + ...
                                      training_rate * new_target_features;
            
        end
        
        function features = get_features(tracker, target, frame)
            if ndims(frame) == 3
                frame = rgb2gray(frame);
            end
            
            patch = target.crop(frame);
            patch = im2double(patch);
            patch = imresize(patch, size(tracker.cosine_window));

            features = patch - mean(patch(:));
            
            
            features = features .* tracker.cosine_window;
            features = fft2(features);
        end
        
        function padded_target = padded_target(tracker)
            padded_target = tracker.target.candidate_target(0, 0, tracker.props.padding);
            padded_target.height = ceil(padded_target.height);
            padded_target.width = ceil(padded_target.width);
        end
        
    end
    
end

