function [ labels ] = create_target_labels( target, sigma )
    % Creates a gaussian peak in the center of the target with specified
    % sigma, and returns the fourier transform of the result.
    gaussian = fspecial('gaussian', [target.height, target.width], sigma);
    gaussian = gaussian ./ max(gaussian(:)); % Normalise so the peak is 1
    gaussian = ifftshift(gaussian);
    
    labels = fft2(gaussian);
end

