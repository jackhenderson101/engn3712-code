function show_video( test_name, dataset_name, save_frames )
    load(['results/' test_name '/' dataset_name '.mat'])
    load(['datasets/' dataset_name '/dataset.mat'], 'dataset')
    if nargin < 3
        save_frames = [];
    end

    colours = {'r', 'b', 'g'};
    figure(1);
    clf;
    figure(2);
    clf
    hold on;
    labels = cell(1, length(trackers)+1);

    for i = 1:length(trackers)
        plot(nan, nan, colours{1+mod(i-1, length(colours))});
        labels{i} = trackers(i).name;
    end
    plot(nan, nan, 'k');
    labels{end} = 'Ground Truth';
    legend(labels);
    while dataset.has_next_frame()

        figure(1);
        frame = dataset.next_frame();
        imshow(frame, 'Border', 'tight');
        gt = dataset.ground_truth.target(dataset.current_frame);
        plot(gt, 'k');
        for i = 1:length(trackers)
            target = trackers(i).history.target(dataset.current_frame);
            plot(target, colours{1+mod(i-1, length(colours))});

        end


        drawnow();
        if any(dataset.current_frame == save_frames)
            [~, ~, ~] = mkdir(['results/' test_name '/plots']);
            filename = sprintf(['results/' test_name '/plots/' dataset_name '-frame-%d.eps'], dataset.current_frame);
            print(filename, '-depsc2', '-loose')
        end

    end
    if ~isempty(save_frames)
        figure(2)
        filename = sprintf(['results/' test_name '/plots/' dataset_name '-legend.eps'], dataset.current_frame);
        print(filename, '-depsc2', '-loose')
    end

end

