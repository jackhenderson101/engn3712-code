#include "Tracker.h"
#include "Config.h"
#include <iostream>
#include "opencvmex.hpp"
#include "class_handle.hpp"


using namespace cv;
using namespace std;

static Config* config;

void checkInputs(int nrhs, const mxArray *prhs[])
{
//     if ((nrhs < 1) || (nrhs > 2))
//     {
//         mexErrMsgTxt("Incorrect number of inputs. Function expects 1 or 2 inputs.");
//     }
}

void rectangle(Mat& rMat, const FloatRect& rRect, const Scalar& rColour)
{
	IntRect r(rRect);
	rectangle(rMat, Point(r.XMin(), r.YMin()), Point(r.XMax(), r.YMax()), rColour);
}



void constructObject(mxArray *plhs[], const mxArray *prhs[]){
    
    const mxArray *props = prhs[1];
    const mxArray *bbox  = prhs[2];
    Mat imgFromMatlab = *ocvMxArrayToImage_uint8(prhs[3], true);

    
    // string configPath = "config.txt";
    config = new Config(props);
    
    if (config->features.size() == 0)
	{
		mexErrMsgTxt("error: no features specified in config\n");
		return;
	}
    
    double *bbox_array = mxGetPr(bbox);
    
    float xmin = (float)bbox_array[1];
    float ymin = (float)bbox_array[0];
    float width = (float)bbox_array[3];
    float height = (float)bbox_array[2];
    FloatRect initBB = FloatRect(xmin, ymin, width, height);
    
    printf("OpenCV Version: %s\n", CV_VERSION);
    config->printConfig();

    Tracker *tracker = new Tracker(*config);
    tracker->Initialise(imgFromMatlab, initBB);
    srand(config->seed);
    tracker->Track(imgFromMatlab);
	if (config->debugMode) {
        namedWindow("result");
    	tracker->Debug();
	}



    plhs[0] = convertPtr2Mat<Tracker>(tracker);
}

void track(mxArray *plhs[], const mxArray *prhs[]){
    Tracker *tracker = convertMat2Ptr<Tracker>(prhs[1]);
    cv::Mat img = *ocvMxArrayToImage_uint8(prhs[2], true);
    tracker->Track(img);
	if (config->debugMode) {
		tracker->Debug();
		Mat result(img.rows, img.cols, CV_8UC3);
		cvtColor(img, result, CV_GRAY2RGB);
		rectangle(result, tracker->GetBB(), CV_RGB(0, 0, 255));
		imshow("result", result);
		waitKey(1);
	}
	

	FloatRect tracker_bbox = tracker->GetBB();

	plhs[0] = mxCreateDoubleMatrix(1, 4, mxREAL);
	double *matlab_bbox = mxGetPr(plhs[0]);
	matlab_bbox[0] = (double)tracker_bbox.YMin() + tracker_bbox.Height()/2;
	matlab_bbox[1] = (double)tracker_bbox.XMin() + tracker_bbox.Width()/2;
	matlab_bbox[2] = (double)tracker_bbox.Height();
	matlab_bbox[3] = (double)tracker_bbox.Width();


}


void exitFcn(const mxArray *prhs[])
{
    destroyObject<Tracker>(prhs[1]);
    delete config;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{  	
    checkInputs(nrhs, prhs);
    const char *funcToCall = mxIsChar(prhs[0]) ? mxArrayToString(prhs[0]) : NULL;

    if (funcToCall != NULL) 
    {
        if (strcmp (funcToCall,"construct") == 0)
            constructObject(plhs, prhs);
        else if (strcmp (funcToCall,"track") == 0)
            track(plhs, prhs);
        else if (strcmp (funcToCall,"destroy") == 0)
            exitFcn(prhs);
        else if (strcmp (funcToCall,"unlock") == 0)
            mexUnlock();

        // Free memory allocated by mxArrayToString
        mxFree((void *)funcToCall);
    }
}

