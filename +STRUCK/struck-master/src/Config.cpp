/* 
 * Struck: Structured Output Tracking with Kernels
 * 
 * Code to accompany the paper:
 *   Struck: Structured Output Tracking with Kernels
 *   Sam Hare, Amir Saffari, Philip H. S. Torr
 *   International Conference on Computer Vision (ICCV), 2011
 * 
 * Copyright (C) 2011 Sam Hare, Oxford Brookes University, Oxford, UK
 * 
 * This file is part of Struck.
 * 
 * Struck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Struck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Struck.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "Config.h"
#include "opencvmex.hpp"
#include "matrix.h"


#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;

Config::Config(const mxArray* props){
    SetDefaults();
    if(!mxIsStruct(props)){
        mexErrMsgTxt("Config params is not a struct");
    }
    mxArray *field;
    
    field = mxGetField(props, 0, "quietMode");
    if(field != NULL){
        quietMode = mxIsLogicalScalarTrue(field);
    }
        
    field = mxGetField(props, 0, "debugMode");
    if(field != NULL){
        debugMode = mxIsLogicalScalarTrue(field);
    }
	
    field = mxGetField(props, 0, "frameWidth");
    if(field != NULL){
        frameWidth = (int)mxGetScalar(field);
    }
    
    field = mxGetField(props, 0, "frameHeight");
    if(field != NULL){
        frameHeight = (int)mxGetScalar(field);
    }
	
    field = mxGetField(props, 0, "seed");
    if(field != NULL){
        seed = (int)mxGetScalar(field);
    }
    
    field = mxGetField(props, 0, "searchRadius");
    if(field != NULL){
        searchRadius = (int)mxGetScalar(field);
    }
    
    field = mxGetField(props, 0, "svmC");
    if(field != NULL){
        svmC = (double)mxGetScalar(field);
    }
    
    field = mxGetField(props, 0, "svmBudgetSize");
    if(field != NULL){
        svmBudgetSize = (int)mxGetScalar(field);
    }
    
    
    string featureName, kernelName;
    double param;
    
    field = mxGetField(props, 0, "feature");
    if(field != NULL){
        featureName = mxArrayToString(field);
        printf("fn: %s\n", featureName);
    } else {
        featureName = "raw";
    }
    
    field = mxGetField(props, 0, "kernel");
    if(field != NULL){
        kernelName = mxArrayToString(field);
        printf("kn: %s\n", kernelName);
    } else {
        kernelName = "linear";
    }
    
    field = mxGetField(props, 0, "featureParam");
    if(field != NULL){
        param = (double)mxGetScalar(field);
    } else {
        param = 0.2;
    }
    

    FeatureKernelPair fkp;

    if      (featureName == FeatureName(kFeatureTypeHaar)) fkp.feature = kFeatureTypeHaar;
    else if (featureName == FeatureName(kFeatureTypeRaw)) fkp.feature = kFeatureTypeRaw;
    else if (featureName == FeatureName(kFeatureTypeHistogram)) fkp.feature = kFeatureTypeHistogram;
    else
    {
        mexErrMsgTxt("unrecognised feature");
    }

    if      (kernelName == KernelName(kKernelTypeLinear)) fkp.kernel = kKernelTypeLinear;
    else if (kernelName == KernelName(kKernelTypeIntersection)) fkp.kernel = kKernelTypeIntersection;
    else if (kernelName == KernelName(kKernelTypeChi2)) fkp.kernel = kKernelTypeChi2;
    else if (kernelName == KernelName(kKernelTypeGaussian))
    {
        fkp.kernel = kKernelTypeGaussian;
        fkp.params.push_back(param);
    }
    else
    {
        mexErrMsgTxt("unrecognised kernel");
    }

    features.push_back(fkp);
    
}

Config::Config(const std::string& path)
{
	SetDefaults();
	
	ifstream f(path.c_str());
	if (!f)
	{
		cout << "error: could not load config file: " << path << endl;
		return;
	}
	
	string line, name, tmp;
	while (getline(f, line))
	{
		istringstream iss(line);
		iss >> name >> tmp;
		
		// skip invalid lines and comments
		if (iss.fail() || tmp != "=" || name[0] == '#') continue;
		
		if      (name == "seed") iss >> seed;
		else if (name == "quietMode") iss >> quietMode;
		else if (name == "debugMode") iss >> debugMode;
		else if (name == "sequenceBasePath") iss >> sequenceBasePath;
		else if (name == "sequenceName") iss >> sequenceName;
		else if (name == "resultsPath") iss >> resultsPath;
		else if (name == "frameWidth") iss >> frameWidth;
		else if (name == "frameHeight") iss >> frameHeight;
		else if (name == "searchRadius") iss >> searchRadius;
		else if (name == "svmC") iss >> svmC;
		else if (name == "svmBudgetSize") iss >> svmBudgetSize;
		else if (name == "feature")
		{
			string featureName, kernelName;
			double param;
			iss >> featureName >> kernelName >> param;
			
			FeatureKernelPair fkp;
			
			if      (featureName == FeatureName(kFeatureTypeHaar)) fkp.feature = kFeatureTypeHaar;
			else if (featureName == FeatureName(kFeatureTypeRaw)) fkp.feature = kFeatureTypeRaw;
			else if (featureName == FeatureName(kFeatureTypeHistogram)) fkp.feature = kFeatureTypeHistogram;
			else
			{
				cout << "error: unrecognised feature: " << featureName << endl;
				continue;
			}
			
			if      (kernelName == KernelName(kKernelTypeLinear)) fkp.kernel = kKernelTypeLinear;
			else if (kernelName == KernelName(kKernelTypeIntersection)) fkp.kernel = kKernelTypeIntersection;
			else if (kernelName == KernelName(kKernelTypeChi2)) fkp.kernel = kKernelTypeChi2;
			else if (kernelName == KernelName(kKernelTypeGaussian))
			{
				if (iss.fail())
				{
					cout << "error: gaussian kernel requires a parameter (sigma)" << endl;
					continue;
				}
				fkp.kernel = kKernelTypeGaussian;
				fkp.params.push_back(param);
			}
			else
			{
				cout << "error: unrecognised kernel: " << kernelName << endl;
				continue;
			}
			
			features.push_back(fkp);
		}
	}
}

Config::~Config()
{
	printf("Destructor for config called");
}

void Config::SetDefaults()
{

	quietMode = false;
	debugMode = true;
	
	sequenceBasePath = "";
	sequenceName = "";
	resultsPath = "";
	
	frameWidth = 320;
	frameHeight = 240;
	
	seed = 0;
	searchRadius = 30;
	svmC = 1.0;
	svmBudgetSize = 100;
	
	features.clear();
}

std::string Config::FeatureName(FeatureType f)
{
	switch (f)
	{
	case kFeatureTypeRaw:
		return "raw";
	case kFeatureTypeHaar:
		return "haar";
	case kFeatureTypeHistogram:
		return "histogram";
	default:
		return "";
	}
}

std::string Config::KernelName(KernelType k)
{
	switch (k)
	{
	case kKernelTypeLinear:
		return "linear";
	case kKernelTypeGaussian:
		return "gaussian";
	case kKernelTypeIntersection:
		return "intersection";
	case kKernelTypeChi2:
		return "chi2";
	default:
		return "";
	}
}

void Config::printConfig(){
    printf("Config Paramters for STRUCK\n");
    printf("Frame Width:     %d\n", frameWidth);
    printf("Frame Height:    %d\n", frameHeight);
    printf("Seed:            %d\n", seed);
    printf("Search Radius:   %d\n", searchRadius);
    printf("svmC:            %.3f\n", svmC);
    printf("SVM Budget Size: %d\n", svmBudgetSize);
    
    for (int i = 0; i < (int)features.size(); ++i)
	{
        printf("Feature %d: %s\n", i, Config::FeatureName(features[i].feature));
        printf("   Kernel: %s\n", Config::KernelName(features[i].kernel));
		if (features[i].params.size() > 0)
		{
			printf("   Params:");
			for (int j = 0; j < (int)features[i].params.size(); ++j)
			{
				printf(" %.3f,", features[i].params[j]);
			}
            printf("\n");
		}
	}
}

ostream& operator<< (ostream& out, const Config& conf)
{
	out << "config:" << endl;
	out << "  quietMode          = " << conf.quietMode << endl;
	out << "  debugMode          = " << conf.debugMode << endl;
	out << "  sequenceBasePath   = " << conf.sequenceBasePath << endl;
	out << "  sequenceName       = " << conf.sequenceName << endl;
	out << "  resultsPath        = " << conf.resultsPath << endl;
	out << "  frameWidth         = " << conf.frameWidth << endl;
	out << "  frameHeight        = " << conf.frameHeight << endl;
	out << "  seed               = " << conf.seed << endl;
	out << "  searchRadius       = " << conf.searchRadius << endl;
	out << "  svmC               = " << conf.svmC << endl;
	out << "  svmBudgetSize      = " << conf.svmBudgetSize << endl;
	
	for (int i = 0; i < (int)conf.features.size(); ++i)
	{
		out << "  feature " << i << endl;
		out << "    feature: " << Config::FeatureName(conf.features[i].feature) << endl;
		out << "    kernel:  " << Config::KernelName(conf.features[i].kernel) <<endl;
		if (conf.features[i].params.size() > 0)
		{
			out << "    params: ";
			for (int j = 0; j < (int)conf.features[i].params.size(); ++j)
			{
				out << " " << conf.features[i].params[j];
			}
			out << endl;
		}
	}
	
	return out;
}

