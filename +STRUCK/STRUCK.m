classdef STRUCK < Tracker
    %KCF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        target
        props
        CPP_Tracker
    end
    
    methods(Static)
        function p = default_properties()
            p = Tracker.base_properties();
            p.seed = 20;
            p.searchRadius = 30;
            p.svmC = 1.0;
            p.svmBudgetSize = 100; 
            p.debugMode = false;
        end
    end
    
    methods
        function tracker = STRUCK()
            
        end
        
        function init(tracker, frame, target, props)
            fprintf('Initialising STRUCK');
            if size(frame,3) == 3
                frame = rgb2gray(frame);
            end
            
            props.frameWidth = size(frame, 2);
            props.frameHeight = size(frame, 1);
            
            tracker.CPP_Tracker = StruckOCV('construct', props, target.bounding_box_reverse(), frame);
            tracker.target = target;
            % Init parameters
            tracker.props = props;
            
            % Init the STRUCK Tracker\
            
            
        end
        
        function update(tracker, frame)
            if size(frame,3) == 3
                frame = rgb2gray(frame);
            end
            
            bbox = StruckOCV('track', tracker.CPP_Tracker, frame);
            tracker.target = Target(bbox(1), bbox(2), bbox(3), bbox(4));
            

        end
        
        function finish(tracker)
            StruckOCV('destroy', tracker.CPP_Tracker);
        end
        
    end
    
end

