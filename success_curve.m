function [success_threshold] = success_curve( overlap, line_spec )
    min_overlap = 0;
    success_measure = 0.4;

    overlap(isnan(overlap)) = [];
    threshold = sort(overlap, 'descend');
    threshold(threshold < min_overlap) = [];
    threshold(isnan(threshold)) = [];
    threshold(end+1) = min_overlap;
    success = linspace(0,length(threshold)/length(overlap),length(threshold));
    success(end) = success(end -1);
    plot(threshold, success, line_spec, 'LineWidth', 2);
    xlabel('Threshold');
    ylabel('Fraction of successfully tracked frames');
    ylim([0,1])
    
    success_threshold = success(find(threshold>success_measure, 1, 'last' ));

end

